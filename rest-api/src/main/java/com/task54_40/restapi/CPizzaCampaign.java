package com.task54_40.restapi;


import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class CPizzaCampaign {
	@CrossOrigin
	@GetMapping("/devcamp-date")
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE")
			.localizedBy(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		return String.format("Hello pizza lover! Hôm nay %s, mua 1 tặng 1.", dtfVietnam.format(today));
	}
    @CrossOrigin
	@GetMapping("/devcamp-lucky-dice/{username}/{firstname}/{lastname}")
	public String getDice(@PathVariable String username
                        , @PathVariable String firstname
                        , @PathVariable String lastname) {
		int randomDiceNum = 1 + (int) (Math.random() * 5);
		return String.format("Xin chào %s! Số may mắn hôm nay của bạn là:", username, randomDiceNum);
	}
}